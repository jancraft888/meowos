#!/usr/bin/env sh

url="https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2023-02-22/2023-02-21-raspios-bullseye-arm64-lite.img.xz"

if [ $# -ne 0 ]; then
  url=$1
fi

curl --progress-bar -LO $url
