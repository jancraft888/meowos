echo [+] downloading
./download_image.sh
echo [+] expanding
./expand_rootfs.sh
echo [+] mounting
./mount_rootfs.sh
echo [+] patching
./patch.sh
echo [+] chrooting
./chroot.sh
echo [+] unmounting
./unmount_rootfs.sh
echo [+] done
