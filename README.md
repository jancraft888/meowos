
# meowOS
The Linux distro for the cutest cats!
This repo contains a distro builder that modifies an `.img` file (Raspberry Pi OS) by mounting it and applying modifications.

> The script probably works on other Debian-based distros with minor modifications.

## meowUI
meowUI is the WIP desktop environment built for meowOS.

## Credits
 * original idea by **meowOS#0919** on the palera1n discord
 * base OS is [Raspberry Pi OS (64-bit)](https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-64-bit)
 * fetch program is a modified version of [ufetch](https://gitlab.com/jschx/ufetch)
 * default shell is [Zsh](https://www.zsh.org) and the prompt is [flatline](https://gitlab.com/jancraft888/flatline)
 * package manager is [APT](https://wiki.debian.org/Apt) and *pacman* using [pacapt](https://github.com/icy/pacapt)

