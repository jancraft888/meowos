#!/bin/sh
#
# based on ufetch
# fetch - tiny system info for meowOS

## INFO

# user is already defined
host="$(hostname)"
os='meowOS'
kernel="$(uname -sr)"
uptime="$(uptime | awk -F, '{sub(".*up ",x,$1);print $1}' | sed -e 's/^[ \t]*//')"
packages="$(dpkg -l | grep -c ^i)"
shell="$(basename "${SHELL}")"

## UI DETECTION

parse_rcs() {
	for f in "${@}"; do
		wm="$(tail -n 1 "${f}" 2> /dev/null | cut -d ' ' -f 2)"
		[ -n "${wm}" ] && echo "${wm}" && return
	done
}

rcwm="$(parse_rcs "${HOME}/.xinitrc" "${HOME}/.xsession")"

ui='tty'
uitype='ui'
if [ -n "${DE}" ]; then
	ui="${DE}"
	uitype='de'
elif [ -n "${WM}" ]; then
	ui="${WM}"
	uitype='wm'
elif [ -n "${XDG_CURRENT_DESKTOP}" ]; then
	ui="${XDG_CURRENT_DESKTOP}"
	uitype='de'
elif [ -n "${DESKTOP_SESSION}" ]; then
	ui="${DESKTOP_SESSION}"
	uitype='de'
elif [ -n "${rcwm}" ]; then
	ui="${rcwm}"
	uitype='wm'
elif [ -n "${XDG_SESSION_TYPE}" ]; then
	ui="${XDG_SESSION_TYPE}"
fi

ui="$(basename "${ui}")"

## DEFINE COLORS

bold='\033[1m'
black='\033[0;30m'
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
blue='\033[0;34m'
magenta='\033[0;35m'
cyan='\033[0;36m'
white='\033[0;37m'
reset='\033[0m'

# you can change these
lc="${reset}${bold}${blue}"         # labels
nc="${reset}${bold}${yellow}"       # user and hostname
ic="${reset}"                       # info
c0="${reset}${magenta}"             # first color

## OUTPUT

printf "
${c0}              ${nc}${USER}${ic}@${nc}${host}${reset}
${c0}   ／l、      ${lc}os         ${ic}${os}${reset}
${c0} （ﾟ､ ｡７     ${lc}kernel     ${ic}${kernel}${reset}
${c0}   l、ﾞ~ヽ    ${lc}uptime     ${ic}${uptime}${reset}
${c0}   じしf_,)ノ ${lc}pkgs       ${ic}${packages}${reset}
${c0}              ${lc}shell      ${ic}${shell}${reset}
${c0}              ${lc}${uitype}         ${ic}${ui}${reset}

"
