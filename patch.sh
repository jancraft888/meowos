#!/usr/bin/env sh

# OS release
echo patching os-release
cat <<EOT >/mnt/etc/os-release
PRETTY_NAME="meowOS GNU/Linux 11"
NAME="meowOS GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
EOT

# zsh
echo installing zsh
unshare --uts chroot /mnt /usr/bin/env apt install -y zsh
unshare --uts chroot /mnt /usr/bin/env chsh -s /usr/bin/zsh root

# meow user
echo setting up meow user
unshare --uts chroot /mnt /usr/bin/env useradd -m -s /usr/bin/zsh meow
unshare --uts chroot /mnt /bin/sh -c "echo \"meow:meow\" | chpasswd"
unshare --uts chroot /mnt /usr/bin/env usermod -aG sudo meow

# flatline
echo installing flatline
curl -Ls https://gitlab.com/jancraft888/flatline/-/raw/main/prompt.sh -o /mnt/usr/bin/flatline_prompt
chmod +x /mnt/usr/bin/flatline_prompt

# zshrc
echo setting up zsh
tee /mnt/home/meow/.zshrc /mnt/root/.zshrc <zshrc >/dev/null
chmod +x /mnt/home/meow/.zshrc
chmod +x /mnt/root/.zshrc
unshare --uts chroot /mnt /usr/bin/env chown meow:meow /home/meow/.zshrc

# fetch
echo installing fetch program
cp fetch.sh /mnt/sbin/fetch
chmod +x /mnt/sbin/fetch

# setting up hostname
echo setting up hostname
unshare --uts chroot /mnt /usr/bin/env hostname meowOS
echo "meowOS" > /mnt/etc/hostname
sed -i 's/raspberrypi/meowOS/g' /mnt/etc/hosts

# installing pacapt
echo installing pacapt
unshare --uts chroot /mnt /usr/bin/env curl -Lso /usr/bin/pacman https://github.com/icy/pacapt/raw/ng/pacapt
unshare --uts chroot /mnt /usr/bin/env chmod 755 /usr/bin/pacman
