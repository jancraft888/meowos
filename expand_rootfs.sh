#!/usr/bin/env sh

compressed=2023-02-21-raspios-bullseye-arm64-lite.img.xz
if [ $# -ne 0 ]; then
  compressed=$1
fi

xz --decompress --keep $compressed
