#!/usr/bin/env sh

losetup -o 272629760 /dev/loop0 2023-02-21-raspios-bullseye-arm64-lite.img
fsck -fv /dev/loop0
mount -o rw /dev/loop0 /mnt
